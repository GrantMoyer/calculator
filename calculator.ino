#include "stack.h"
#include "math.h"
#include <LiquidCrystal.h> //drives lcd
#include <stdio.h>

//maps keypad pins to their corresponding pic pins
const byte kp2ard[] = {8, 255, 7, 6, 5, 4, 2, 3, 1, 0}; //pin 1 isn't used

//the keypad pins corresponding to the keypad rows and columns
const byte kp_rows[] = {9, 8, 6, 7};
const byte kp_cols[] = {5, 4, 3};

//keypad special keys pins
const byte kp_asterisk = 2;
const byte kp_pound = 0;

//LCD control pins in pic pin
const byte lcd_rs = 14;
const byte lcd_en = 15;

//LCD data pins in pic pin
const byte lcd_db4 = 9;
const byte lcd_db5 = 10;
const byte lcd_db6 = 11;
const byte lcd_db7 = 12;

LiquidCrystal lcd(lcd_rs, lcd_en, lcd_db4, lcd_db5, lcd_db6, lcd_db7);

//data stack
Stack stack;

void setup() {
  // put your setup code here, to run once:

  //set all rows to high ouput
  for (byte r = 0; r < 4; ++r) {
    byte pin = kp2ard[kp_rows[r]];
    pinMode(pin, OUTPUT);
    digitalWrite(pin, HIGH);
  }

  //set all columns to pull-up input
  for (byte c = 0; c < 3; ++c) {
    byte pin = kp2ard[kp_cols[c]];
    pinMode(pin, INPUT_PULLUP);
  }

  //set special keys to pull-up input
  pinMode(kp2ard[kp_asterisk], INPUT_PULLUP);
  pinMode(kp2ard[kp_pound], INPUT_PULLUP);

  //lcd dimensions
  lcd.begin(8, 2); //16 columns, 1 row, but represented as two rows of 8.

  stack = Stack(0.0); //start the stack
  print_num(*stack.get_top());
}

//polls the keypad's numbers, and returns a number if it is being pressed.
//If no number is being pressed, returns -1.
char poll_numpad() {
  //the layout of the keypad
  static const char kp[][3] = {{ 1,  2,  3},
                               { 4,  5,  6},
                               { 7,  8,  0},
                               {-1, -1,  9}};

  //poll all numbers
  byte r; //row index for loop. needs to be in this scope
  byte c; //column index for loop. needs to be in this scope
  for (r = 0; r < 4; ++r) { //loop through all rows of keypad
    byte row_pin = kp2ard[kp_rows[r]];
    digitalWrite(row_pin, LOW); // turn row off to test colums in row for input
    for (c = 0; c < 3; ++c) { //loop through all columns of keypad
      byte col_pin = kp2ard[kp_cols[c]];
      //debounce
      if (digitalRead(col_pin) == LOW) {
        unsigned long start = millis(); //get sart time
        while (digitalRead(col_pin) == LOW); //wait until button not pressed
        if (millis() - start >= 5) { //buton pressed for at least 5 milliseconds
          goto breakloop; //label is right after loop, don't freak out.
        }
      }
    }
    digitalWrite(row_pin, HIGH); // turn row back on
  }
  breakloop: //this is a label, in case you forgot
  if (r < 4) digitalWrite(kp2ard[kp_rows[r]], LOW); // turn row back on, since the loop broke before doing that

  char number = -1; //-1 indicate number not pressed unless changed in following if statement
  if (r < 4) { //broke out of loop
    number = kp[r][c]; //set number to the pressed key
  }

  return number;
}

//Returns true if the asterisk key is being pressed
boolean poll_asterisk() {
  const byte pin = kp2ard[kp_asterisk];
  //debounce
  if (digitalRead(pin) == LOW) {
    unsigned long start = millis(); //get sart time
    while (digitalRead(pin) == LOW); //wait until button not pressed
    if (millis() - start >= 5) { //buton pressed for at least 5 milliseconds
      return true;
    }
  }
  return false;
}

//returns true if the pound key is being pressed
boolean poll_pound() {
  const byte pin = kp2ard[kp_pound];
  //debounce
  if (digitalRead(pin) == LOW) {
    unsigned long start = millis(); //get sart time
    while (digitalRead(pin) == LOW); //wait until button not pressed
    if (millis() - start >= 5) { //buton pressed for at least 5 milliseconds
      return true;
    }
  }
  return false;
}

void print_num(double num) {
  char str[17] = {0};

  dtostrf(num, 16, 7, str);

  char first[9]; //first half of string
  strncpy(first, str, 8);
  first[8] = 0;
  
  char second[9]; //second half of string
  strncpy(second, str + 8, 8);
  second[8] = 0;

  lcd.clear();
  lcd.print(first);
  lcd.setCursor(0,1);
  lcd.print(second);
}

int decimal = 0; //the place of the decimal to be entered
boolean oped = false; //whether or not the last button press was an operation
void loop() {
  // put your main code here, to run repeatedly:
  char number = poll_numpad();

  //get special keys flag
  boolean asterisk = poll_asterisk();
  boolean pound = poll_pound();

  if (number < 0) { //no number pressed
    if (pound) {
      stack.push(0.0); //start new number
      decimal = 0; //enter normal digits
      print_num(*stack.get_top());
      oped = false;
    } else if (asterisk) {
      if (oped) {
        stack.push(0.0); //start new number
        decimal = 0; //enter normal digits
        oped = false;
      }
      decimal = 1; //enter decimal places
      print_num(*stack.get_top());
    }
  } else { //number pressed
    if (!pound) { //use number as input
      if (oped) {
        stack.push(0.0); //start new number
        decimal = 0; //enter normal digits
        oped = false;
      }
      double *top = stack.get_top();
      if (decimal > 0) { //entering decimal digit
        *top += number * pow(10, -decimal);
        ++decimal;
      } else { //entering normal digit
        *top *= 10;
        *top += number;
      }
    } else { //use number as operation
      double *top = stack.get_top();
      double old_top;
      switch (number) {
        case 0: //cos
          *top = cos(*top / 180.0 * M_PI); //cosine in degress
          break;
        case 1: //+
          old_top = stack.pop();
          *stack.get_top() += old_top;
          break;
        case 2: //-
          *top = -*top;
          break;
        case 3: //exp
          *top = exp(*top);
          break;
        case 4: //*
          old_top = stack.pop();
          *stack.get_top() *= old_top;
          break;
        case 5: //inv
          *top = 1 / *top;
          break;
        case 6: //ln
          *top = log(*top);
          break;
        case 7: //sqr
          *top *= *top;
          break;
        case 8: //sqrt
          *top = sqrt(*top);
          break;
        case 9: //sin
          *top = sin(*top / 180.0 * M_PI); //cosine in degress
          break;
      }
      oped = true;
      decimal = 0; //reset to normal enter mode
    }
    print_num(*stack.get_top());
  }
}
