#ifndef STACK_H
#define STACK_H

#include "Arduino.h"

class Stack {
  private:
    struct StackFrame {
      StackFrame(double new_value, StackFrame *new_below) : value(new_value), below(new_below) {}
      double value;
      StackFrame *below;
    };
    
  public:
    Stack() : top(NULL), size(0) {}
    Stack(double initial_value) : top(new StackFrame(initial_value, NULL)), size(1) {}
    
    void push(double value);
    double pop();

    double *get_top();
    
    long get_size();
    
  private:
    StackFrame *top;
    long size;
};

#endif
