#include "stack.h"
#include "math.h"

void Stack::push(double value) {
  top = new StackFrame(value, top);
  ++size;
}

double Stack::pop() {
  if (get_size() > 0) {
    double popped = top->value;
    
    StackFrame *old_top = top;
    top = top->below;
    delete old_top;
    --size;
    
    return popped;
  } else return NAN;
}

double *Stack::get_top() {
  return &top->value;
}

long Stack::get_size() {
  return size;
}

